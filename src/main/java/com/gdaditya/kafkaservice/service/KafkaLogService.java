package com.gdaditya.kafkaservice.service;

public interface KafkaLogService {
    void listenLog(String message);
}
