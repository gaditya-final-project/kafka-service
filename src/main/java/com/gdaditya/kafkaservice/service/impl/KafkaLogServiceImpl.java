package com.gdaditya.kafkaservice.service.impl;

import com.gdaditya.kafkaservice.model.LogEntity;
import com.gdaditya.kafkaservice.service.KafkaLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class KafkaLogServiceImpl implements KafkaLogService {

    private final MongoTemplate mongoTemplate;

    @KafkaListener(topics = "log", groupId = "final_project")
    public void listenLog(String message) {
        mongoTemplate.save(new LogEntity(message, new Date()));
    }
}
